﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

[ExecuteInEditMode]
public class WaypointController : MonoBehaviour
{
    private static WaypointController _instance;

    [SerializeField] private Transform _waypointParent1;
    [SerializeField] private Transform _waypointParent2;
    [SerializeField] private Transform _waypointParent3;
    [SerializeField] private GameObject _waypoint;

    //List<GameObject> Waypoints1 = new List<GameObject>();
    //List<GameObject> Waypoints2 = new List<GameObject>();
    //List<GameObject> Waypoints3 = new List<GameObject>();

    List<float> xList = new List<float>();
    List<float> yList = new List<float>();
    List<float> zList = new List<float>();

    string path1 = @"C:\Users\Алексей\Downloads\Test_3d_prog\curve0.txt";
    string path2 = @"C:\Users\Алексей\Downloads\Test_3d_prog\curve1.txt";
    string path3 = @"C:\Users\Алексей\Downloads\Test_3d_prog\curve2.txt";

    public static WaypointController Instance
    {
        get
        {
            if (_instance == null)
            {
                var go = new GameObject("WaypointController", typeof(WaypointController));

                _instance = go.GetComponent<WaypointController>();
            }

            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;
    }

    private void GetCoordsFromFile(string path)
    {
        xList.Clear();
        yList.Clear();
        zList.Clear();
        string file = "";
        int index = 0;
        using (StreamReader sr = new StreamReader(path))
        {
            file = sr.ReadToEnd();
            StringBuilder sb = new StringBuilder();
            sb.Append(file);
            sb.Replace(",", "");
            sb.Replace("(", "");
            sb.Replace(")", "");
            sb.Replace("[", "");
            sb.Replace("]", "");
            var replace = sb.ToString();
            var replaceList = replace.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            foreach (var s in replaceList)
            {
                var substring = s.Trim();
                float d;
                d = float.Parse(substring);
                switch (index++)
                {
                    case 0:
                        xList.Add(d);
                        break;
                    case 1:
                        yList.Add(d);
                        break;
                    case 2:
                        zList.Add(d);
                        index = 0;
                        break;
                }
            }
        }
    }

    public void SpawnWaypoints()
    {
        _waypointParent1 = new GameObject("Curve 1").transform;
        //_waypointParent1 = Instantiate(parent).transform;
        _waypointParent1.parent = gameObject.transform;
        GetCoordsFromFile(path1);
        for (int i = 0; i < xList.Count; i += 10)
        {
            var go = Instantiate(_waypoint, _waypointParent1);
            go.transform.position = new Vector3(xList[i], yList[i], zList[i]);
        }

        _waypointParent2 = new GameObject("Curve 2").transform;
        //_waypointParent2 = Instantiate(parent).transform;
        _waypointParent2.parent = gameObject.transform;
        GetCoordsFromFile(path2);
        for (int i = 0; i < xList.Count; i += 10)
        {
            var go = Instantiate(_waypoint, _waypointParent2);
            go.transform.position = new Vector3(xList[i], yList[i], zList[i]);
        }

        _waypointParent3 = new GameObject("Curve 3").transform;
        //_waypointParent3 = Instantiate(parent).transform;
        _waypointParent3.parent = gameObject.transform;
        GetCoordsFromFile(path3);
        for (int i = 0; i < xList.Count; i += 10)
        {
            var go = Instantiate(_waypoint, _waypointParent3);
            go.transform.position = new Vector3(xList[i], yList[i], zList[i]);
        }
    }

    public void DeleteWaypoints()
    {
        DestroyImmediate(_waypointParent1.gameObject);
        DestroyImmediate(_waypointParent2.gameObject);
        DestroyImmediate(_waypointParent3.gameObject);
    }
}