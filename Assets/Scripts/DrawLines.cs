﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLines : MonoBehaviour
{
    [SerializeField] private Material _lineMat;
    private List<Transform> points1 = new List<Transform>();
    private List<Transform> points2 = new List<Transform>();
    private List<Transform> points3 = new List<Transform>();

    void Start()
    {
        points1 = WaypointManager.Instance.Waypoints1;
        points2 = WaypointManager.Instance.Waypoints2;
        points3 = WaypointManager.Instance.Waypoints3;
    }

    void DrawConnectingLines()
    {
        DrawLine(points1);
        DrawLine(points2);
        DrawLine(points3);
    }

    void DrawLine(List<Transform> points)
    {
        for (int i = 1; i < points.Count; i++)
        {
            GL.Begin(GL.LINES);
            _lineMat.SetPass(0);
            GL.Color(new Color(_lineMat.color.r, _lineMat.color.g, _lineMat.color.b, _lineMat.color.a));
            GL.Vertex3(points[i].position.x, points[i].position.y, points[i].position.z);
            GL.Vertex3(points[i - 1].position.x, points[i - 1].position.y, points[i - 1].position.z);
            GL.End();
        }
    }

    // To show the lines in the game window whne it is running
    void OnPostRender()
    {
        DrawConnectingLines();
    }

    // To show the lines in the editor
    void OnDrawGizmos()
    {
        DrawConnectingLines();
    }
}