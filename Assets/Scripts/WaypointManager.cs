﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaypointManager : MonoBehaviour
{
    private static WaypointManager _instance;

    [SerializeField] private Transform _waypointParent1;
    [SerializeField] private Transform _waypointParent2;
    [SerializeField] private Transform _waypointParent3;



    [NonSerialized] public List<Transform> Waypoints1 = new List<Transform>();
    [NonSerialized] public List<Transform> Waypoints2 = new List<Transform>();
    [NonSerialized] public List<Transform> Waypoints3 = new List<Transform>();


    public static WaypointManager Instance
    {
        get
        {
            //if (_instance == null)
            //{
            //    var go = new GameObject("WaypointManager", typeof(WaypointManager));

            //    _instance = go.GetComponent<WaypointManager>();
            //}

            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;
    }

    // Use this for initialization
    void Start()
    {
        _waypointParent1 = GameObject.Find("Curve 1").transform;
        for (int i = 0; i < _waypointParent1.childCount; i++)
        {
            Waypoints1.Add(_waypointParent1.GetChild(i));
        }
        _waypointParent2 = GameObject.Find("Curve 2").transform;
        for (int i = 0; i < _waypointParent2.childCount; i++)
        {
            Waypoints2.Add(_waypointParent2.GetChild(i));
        }
        _waypointParent3 = GameObject.Find("Curve 3").transform;
        for (int i = 0; i < _waypointParent3.childCount; i++)
        {
            Waypoints3.Add(_waypointParent3.GetChild(i));
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public Transform GetWaypointTransform(int index, WayType wayType)
    {
        if (wayType == WayType.Left && index < Waypoints1.Count)
        {
            return Waypoints1[index];
        }
        if (wayType == WayType.Center && index < Waypoints2.Count)
        {
            return Waypoints2[index];
        }
        if (wayType == WayType.Right && index < Waypoints3.Count)
        {
            return Waypoints3[index];
        }
        return null;
    }
}

public enum WayType
{
    Left,
    Center,
    Right
}