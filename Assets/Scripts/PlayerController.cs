﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerController : NetworkBehaviour
{
    //public static PlayerController Instance;

    [SerializeField] private float _currentSpeed;
    [SerializeField] private float _maxSpeed = 100f;
    [SerializeField] private float _cameraDistance = 8f;
    [SerializeField] private float _cameraHeight = 4f;

    private Text _finishText;

    private int _passedWaypointsCount = 0;
    private Camera _mainCamera;
    private Transform _nextWaypoint;
    private float _minDistanceToWaypoint = 1f;
    private WayType _currentWay;
    

    private Vector3 _cameraOffset;

    void Awake()
    {
        
    }
    // Use this for initialization
    void Start()
    {
        if (!isLocalPlayer)
        {
            //Destroy(_finishText);
            return;
        }
        _finishText = GetComponentInChildren<Text>(true);
        //Instance = this;

        _mainCamera = Camera.main;
        _currentWay = WayType.Left;
        if (transform.position == WaypointManager.Instance.GetWaypointTransform(0, WayType.Right).position)
        {
            _currentWay = WayType.Right;
        }
        if (transform.position == WaypointManager.Instance.GetWaypointTransform(0, WayType.Center).position)
        {
            _currentWay = WayType.Center;
        }
        _cameraOffset = new Vector3(0f, _cameraHeight, -_cameraDistance);
        MoveCamera();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        if (_passedWaypointsCount == 0)
        {
            _nextWaypoint = WaypointManager.Instance.GetWaypointTransform(0, _currentWay);
            gameObject.transform.position = _nextWaypoint.position;
        }
        if (_nextWaypoint == null)
        {
            _currentSpeed = 0;
            return;
        }
        float dist = Vector3.Distance(gameObject.transform.position, _nextWaypoint.position);
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            IncreaseSpeed(5f);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            DecreaseSpeed(5f);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            ShiftToLeft();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            ShiftToRight();
        }
        if (dist > _minDistanceToWaypoint)
        {
            Move();
        }
        else
        {
            _passedWaypointsCount++;
            _nextWaypoint = WaypointManager.Instance.GetWaypointTransform(_passedWaypointsCount, _currentWay);
        }
    }

    void LateUpdate()
    {
        if (isLocalPlayer)
        {
            MoveCamera();

        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (!isLocalPlayer)
        {
            return;
        }
        if (other.GetComponent<Finish>())
        {
            CmdDoFinish(this.gameObject);
        }
        if (other.GetComponent<PlayerController>())
        {
            CmdDoDecreaseSpeed(this.gameObject);
        }
    }
    [Command]
    void CmdDoDecreaseSpeed(GameObject target)
    {
        target.GetComponent<PlayerController>().RpcDecreaseSpeed(1.5f);
    }
    [Command]
    void CmdDoFinish(GameObject sender)
    {
        Finish.Instance.OnFinish(sender);
    }
    [ClientRpc]
    public void RpcOnFinish(int pos, float time)
    {
        if (!isLocalPlayer)
        {
            return;
        }
        for (int i = 0; i < GetComponents<SphereCollider>().Length; i++)
        {
            GetComponent<SphereCollider>().enabled = false;
        }
        _finishText.enabled = true;
        _finishText.text = String.Format("Finished by {0}. Time: {1}", pos, time);
    }

    [ClientRpc]
    public void RpcDecreaseSpeed(float divider)
    {
        if (!isLocalPlayer)
        {
            return;
        }
        _currentSpeed = Mathf.Clamp(_currentSpeed / divider, 0f, _maxSpeed);
    }
    private void Move()
    {
        gameObject.transform.LookAt(_nextWaypoint.transform.position);
        gameObject.transform.position += gameObject.transform.forward * _currentSpeed * Time.deltaTime;
    }

    private void ShiftToLeft()
    {
        if (_currentWay == WayType.Center)
        {
            _currentWay = WayType.Left;
            _nextWaypoint = WaypointManager.Instance.GetWaypointTransform(_passedWaypointsCount, _currentWay);
        }
        if (_currentWay == WayType.Right)
        {
            _currentWay = WayType.Center;
            _nextWaypoint = WaypointManager.Instance.GetWaypointTransform(_passedWaypointsCount, _currentWay);
        }
    }

    private void ShiftToRight()
    {
        if (_currentWay == WayType.Center)
        {
            _currentWay = WayType.Right;
            _nextWaypoint = WaypointManager.Instance.GetWaypointTransform(_passedWaypointsCount, _currentWay);
        }
        if (_currentWay == WayType.Left)
        {
            _currentWay = WayType.Center;
            _nextWaypoint = WaypointManager.Instance.GetWaypointTransform(_passedWaypointsCount, _currentWay);
        }
    }

    private void IncreaseSpeed(float count)
    {
        _currentSpeed = Mathf.Clamp(_currentSpeed + count, 0f, _maxSpeed);
    }

    private void DecreaseSpeed(float count)
    {
        _currentSpeed = Mathf.Clamp(_currentSpeed - count, 0f, _maxSpeed);
    }

    private void MoveCamera()
    {
        _mainCamera.transform.position = transform.position;
        float step = 2f * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(_mainCamera.transform.forward, transform.forward, step, 0.0F);
        _mainCamera.transform.rotation = Quaternion.LookRotation(newDir);

        _mainCamera.transform.Translate(_cameraOffset);
    }
}