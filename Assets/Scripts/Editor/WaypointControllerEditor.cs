﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(WaypointController))]
public class WaypointControllerEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        WaypointController waypointController = (WaypointController)target;
        if (GUILayout.Button("SpawnWaypoints"))
        {
            waypointController.SpawnWaypoints();
        }
        if (GUILayout.Button("DeleteWaypoints"))
        {
            waypointController.DeleteWaypoints();
        }
    }
}
