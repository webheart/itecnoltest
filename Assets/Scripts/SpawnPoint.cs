﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    [SerializeField] private WayType StartWay;

    private bool _isInited = false;
    void Update()
    {
        if (_isInited) return;    
        switch (StartWay)
        {
            case WayType.Left:
                transform.position = WaypointManager.Instance.Waypoints1[0].position;
                break;
            case WayType.Center:
                transform.position = WaypointManager.Instance.Waypoints2[0].position;
                break;
            case WayType.Right:
                transform.position = WaypointManager.Instance.Waypoints3[0].position;
                break;
        }
        _isInited = true;
    }
}