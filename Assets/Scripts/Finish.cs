﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Finish : NetworkBehaviour
{
    public static Finish Instance;
    //[SerializeField] private Text _finishText;
    //[SerializeField] private Text _timeText;

    private int _position = 1;
    private bool _isInited = false;
    private float _startTime;

    void Start()
    {
        Instance = this;
        StartTimer();
    }

    void Update()
    {
        if (!_isInited)
        {
            Init();
            _isInited = true;
        }
    }

    public void StartTimer()
    {
        _startTime = Time.time; ;
        _position = 1;
    }

    public void Init()
    {
        transform.position = WaypointManager.Instance.Waypoints2.Last().position;
    }
    
    public void OnFinish(GameObject sender)
    {
        sender.GetComponent<PlayerController>().RpcOnFinish(_position++, Time.time - _startTime);
    }
}